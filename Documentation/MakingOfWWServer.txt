Making of WWServer
WotWizard Server

Download the Blackbox Component Framework Version 1.7 at:
	
	http://blackboxframework.org/stable/blackbox-1.7.zip

and extract it into the "Blackbox" directory (you may have copied this directory elsewhere before).

Then, run "Blackbox/BlackBox.exe", open this file itself in the BlackBox framework (Blackbox/Duniter/Docu/MakingOf.odc) and execute the commands below, by clicking on the successive following .

 DevCompiler.CompileThis 
CpcUtf8Conv BabelCompil BabelInit BabelInit0 BabelCars BabelDef BabelLexique BabelSyntaxe BabelAnaSem BabelInterface BabelAvl BabelBabel UtilJson BasicsBits BasicsAssert BasicsStrings BasicsFiles DuniterLog UnicodeLetter_Mapping UnicodeCase_Mapping UtilAvlTree DuniterBasic UtilBTree UtilBBTree DuniterBlockchain DuniterSandbox DuniterWotWizard DuniterServer SQLiteHost SQLiteStrings SQLiteDriver 

 DevDebug.UnloadThis 
SQLiteDriver SQLiteStrings SQLiteHost DuniterServer DuniterWotWizard DuniterSandbox DuniterBlockchain UtilBBTree UtilBTree DuniterBasic UtilAvlTree UnicodeCase_Mapping UnicodeLetter_Mapping DuniterLog BasicsFiles BasicsStrings BasicsAssert BasicsBits UtilJson BabelBabel BabelAvl BabelInterface BabelAnaSem BabelSyntaxe BabelLexique BabelDef BabelCars BabelInit0 BabelInit BabelCompil CpcUtf8Conv 

 BabelBabel.CompileThis 
UtilJson 

 DevLinker.Link WWServer.exe := 
Kernel$+ Files HostFiles HostPackedFiles StdLoader
1 Applogo.ico 2 Doclogo.ico 3 SFLogo.ico 4 CFLogo.ico 5 DtyLogo.ico
6 folderimg.ico 7 openimg.ico 8 leafimg.ico
1 Move.cur 2 Copy.cur 3 Link.cur 4 Pick.cur 5 Stop.cur 6 Hand.cur 7 Table.cur

 DevPacker.PackThis WWServer.exe :=
Babel/Code/Compil.ocf Basics/Code/Assert.ocf Basics/Code/Bits.ocf Basics/Code/Files.ocf Basics/Code/Strings.ocf Cpc/Code/Utf8Conv.ocf Duniter/Code/Basic.ocf Duniter/Code/Blockchain.ocf Duniter/Code/Log.ocf Duniter/Code/Sandbox.ocf Duniter/Code/Server.ocf Duniter/Code/WotWizard.ocf Host/Code/CFrames.ocf Host/Code/Clipboard.ocf Host/Code/Cmds.ocf Host/Code/Dialog.ocf Host/Code/Files.ocf Host/Code/Fonts.ocf Host/Code/Mechanisms.ocf Host/Code/Menus.ocf Host/Code/Ports.ocf Host/Code/Printers.ocf Host/Code/Registry.ocf Host/Code/Windows.ocf Ole/Code/Data.ocf SQLite/Code/Driver.ocf SQLite/Code/Host.ocf SQLite/Code/Strings.ocf Sql/Code/DB.ocf Sql/Code/Drivers.ocf Std/Code/Api.ocf Std/Code/CFrames.ocf Std/Code/Cmds.ocf Std/Code/Dialog.ocf Std/Code/Interpreter.ocf Std/Code/Links.ocf Std/Code/Loader.ocf Std/Code/Log.ocf Std/Code/MenuTool.ocf System/Code/Containers.ocf System/Code/Controllers.ocf System/Code/Controls.ocf System/Code/Converters.ocf System/Code/Dates.ocf System/Code/Dialog.ocf System/Code/Documents.ocf System/Code/Files.ocf System/Code/Fonts.ocf System/Code/Init.ocf System/Code/Kernel.ocf System/Code/Log.ocf System/Code/Math.ocf System/Code/Mechanisms.ocf System/Code/Meta.ocf System/Code/Models.ocf System/Code/Ports.ocf System/Code/Printers.ocf System/Code/Printing.ocf System/Code/Properties.ocf System/Code/Sequencers.ocf System/Code/Services.ocf System/Code/Stores.ocf System/Code/Strings.ocf System/Code/Views.ocf System/Code/Windows.ocf Text/Code/Cmds.ocf Text/Code/Controllers.ocf Text/Code/Mappers.ocf Text/Code/Models.ocf Text/Code/Rulers.ocf Text/Code/Setters.ocf Text/Code/Views.ocf Unicode/Code/Case_Mapping.ocf Unicode/Code/Letter_Mapping.ocf Util/Code/AvlTree.ocf Util/Code/BBTree.ocf Util/Code/BTree.ocf Util/Code/Json.ocf

Dev/Code/Browser.ocf
Dev/Code/Commanders.ocf
Dev/Code/Debug.ocf
Duniter/Code/SA-Config.ocf => System/Code/Config.ocf
Duniter/Rsrc/SA-CommandLine.txt => System/Rsrc/CommandLine.txt
Duniter/Rsrc/SA-Menus.odc => System/Rsrc/Menus.odc
Duniter/Rsrc/Strings.odc
Duniter/Rsrc/fr/Strings.odc
Host/Code/Bitmaps.ocf
Host/Code/Pictures.ocf
Host/Code/TextConv.ocf
Ole/Code/Client.ocf
Ole/Code/Server.ocf
Ole/Code/Storage.ocf
Std/Code/ETHConv.ocf
Util/Rsrc/Json.tbl
Xhtml/Code/Exporter.ocf

Copy "BlackBox/WWServer.exe" in an empty directory, download and copy "sqlite3.dll" in the same directory as "WWServer.exe":

	https://www.sqlite.org/2015/sqlite-dll-win32-x86-3081002.zip

It's done.