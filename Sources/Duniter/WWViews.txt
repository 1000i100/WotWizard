(* 
Duniter: WotWizard.

Copyright (C) 2017 GérardMeunier

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License  for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*)

MODULE DuniterWWViews;
	
	
	
	(* This module creates views displaying WotWizard forecasts *)
		
	IMPORT
		
		(*
		StdLog,
		*)
		
		A := UtilAvlTree, BA := DuniterBasic, Containers, Controllers, Controls, DevSearch, Dialog, Documents, Files, Fonts, J := UtilJson, Math, Models, Ports, Properties, Sequencers, Services, StdCmds, Stores, TextMappers, TextModels, TextRulers, TextViews, UtilExternal, Views, Windows;
	
	CONST
		
		calculusPeriod = 5 * 60 * Services.resolution; (* Time between two executions of "WWServer.exe" *)
		verifyPeriod = 5 * Services.resolution; (* Time between two verifications "WWServer.exe" has stopped *)
		
		rsrcDir = "Duniter/Rsrc"; (* Resources directory *)
		runningName = "Running.odc"; (* Name of the (empty) file which exists while "WWServer.exe" is running *)
		
		textMeta = "WWMeta.json"; (* File produced by "WWServer.exe" containing metadata *)
		textByDate = "WWByDate.json"; (* File produced by "WWServer.exe" containing entries data, sorted by dates *)
		
		oldEntriesName = "WW_Old_Entries.odc"; (* File containing previous metadata output *)
		oldMetaName = "WW_Old_Meta.odc"; (* File containing previous entries data output *)
		
		title = "WotWizard"; (* Title of the outputs *)
		
		width = 80 * Ports.mm; (* Original window width *)
		height = 150 * Ports.mm; (* Original window height *)
		
		(* Output sorts *)
		byDate = 0; byName = 1; meta = 2;
		
		(* Sizes of fonts *)
		titleSize = 20 * Fonts.point;
		headSize = 14 * Fonts.point;
		corpusSize = 10 * Fonts.point;
		
		(* Indentations widths *)
		indent = 5 * Ports.mm;
		indentM = 2 * Ports.mm;
		
		(* Extra widths of buttons *)
		radioOffset = 5 * Ports.mm;
		buttonOffset = 5 * Ports.mm;
		
		(* Tab stops *)
		firstCtrlPos = width * 1 DIV 6;
		secondCtrlPos = width * 3 DIV 6;
		thirdCtrlPos = width * 5 DIV 6;
	
	TYPE
		
		(* "WWServer.exe" iteration *)
		NewCalculus = POINTER TO RECORD (Services.Action)
		END;
		
		(* Iteration of the verification of the execution of "WWServer.exe" *)
		VerifyCalcEnd = POINTER TO RECORD (Services.Action)
		END;
		
		(* Type of the view which displays the WotWizard lists *)
		View = POINTER TO LIMITED RECORD (Views.View)
			nowS: BA.DateTime; (* Current date *)
			occurD, occurN: A.Tree; (* Sets of permutations, sorted by dates and by names *)
			meta: J.Json; (* Metadata *)
			modified: BOOLEAN; (* The sets of permutations have been modified *)
			text: TextViews.View; (* The displayed text *)
		END;
		
		(* Return the first View *)
		FindMsg = RECORD (Views.Message)
			v: View;
		END;
		
		(* Entries sorted by date(s) *)
		PropDate = POINTER TO RECORD (A.Elem)
			id: J.StringP; (* uid of a candidate *)
			date: LONGINT; (* A possible date of her entry *)
			after: BOOLEAN; (* Incomplete calculus: the entry is at date or after *)
			proba: INTEGER; (* Probability of this entry *)
		END;
		
		(* Entries sorted by id(s) *)
		PropName = POINTER TO RECORD (A.Elem)
			id: J.StringP; (* uid of a candidate *)
			date: LONGINT; (* A possible date of her entry *)
			after: BOOLEAN; (* Incomplete calculus: the entry is at date or after *)
			proba: INTEGER; (* Probability of this entry *)
		END;
	
	VAR
		
		nC: NewCalculus;
		vC: VerifyCalcEnd;
		running: BOOLEAN; (* "WWServer.exe" is running *)
		
		dateNameMeta*: INTEGER; (* Sort of the output *)
		
		showOldEntries-: BOOLEAN;
	
	(* Comparison procedures for permutations of entries *)
	
	PROCEDURE (p1: PropDate) Compare (p2: A.Elem): BYTE;
		
		BEGIN (*Compare*)
			WITH p2: PropDate DO
				IF p1.date < p2.date THEN
					RETURN A.lt;
				END;
				IF p1.date > p2.date THEN
					RETURN A.gt;
				END;
				RETURN BA.CompP(p1.id, p2.id);
			END;
		END Compare;
	
	PROCEDURE (p1: PropName) Compare (p2: A.Elem): BYTE;
		
		VAR
			
			b: BYTE;
		
		BEGIN (*Compare*)
			WITH p2: PropName DO
				b := BA.CompP(p1.id, p2.id);
				IF b # A.eq THEN
					RETURN b;
				END;
				IF p1.date < p2.date THEN
					RETURN A.lt;
				END;
				IF p1.date > p2.date THEN
					RETURN A.gt;
				END;
				RETURN A.eq;
			END;
		END Compare;
	
	(* Find the first View *)
	PROCEDURE FindFirst (): View;
		
		VAR
			
			w: Windows.Window;
			msg: FindMsg;
		
		BEGIN (*FindFirst*)
			w := Windows.dir.First();
			WHILE w # NIL DO
				IF (w.frame # NIL) & (w.frame.view # NIL) & (w.frame.view IS Documents.Document) & (w.frame.view(Documents.Document).ThisView() IS View) THEN
					RETURN w.frame.view(Documents.Document).ThisView()(View);
				END;
				w := Windows.dir.Next(w);
			END;
			msg.v := NIL;
			Views.Omnicast(msg);
			RETURN msg.v;
		END FindFirst;
	
	PROCEDURE SetNotDirty (v: Views.View);
		
		VAR
			
			d: Stores.Domain;
			any: ANYPTR;
			seq: Sequencers.Sequencer;
		
		BEGIN (*SetNotDirty*)
			ASSERT(v # NIL, 20);
			d := v.Domain();
			IF d # NIL THEN
				any := d.GetSequencer();
				IF (any # NIL) & (any IS Sequencers.Sequencer) THEN
					seq := any(Sequencers.Sequencer);
					seq.SetDirty(FALSE);
				END;
			END;
		END SetNotDirty;
	
	(* Test the equality of the two permutations sorted by dates o1 and o2 *)
	PROCEDURE OccursEqual (o1, o2: A.Tree): BOOLEAN;
		
		VAR
			
			e1, e2: A.Elem;
		
		PROCEDURE Equal (e1, e2: A.Elem): BOOLEAN;
			
			BEGIN (*Equal*)
				WITH e1: PropDate DO
					WITH e2: PropDate DO
						RETURN (e1.id$ = e2.id$) & (e1.date = e2.date) & (e1.after = e2.after) & (e1.proba = e2.proba);
					END;
				END;
			END Equal;
		
		BEGIN (*OccursEqual*)
			e1 := o1.Next(NIL); e2 := o2.Next(NIL);
			WHILE (e1 # NIL) & (e2 # NIL) & Equal(e1, e2) DO
				e1 := o1.Next(e1); e2 := o2.Next(e2);
			END;
			RETURN (e1 = NIL) & (e2 = NIL);
		END OccursEqual;
	
	(* Print the permutation occurD sorted by dates *)
	PROCEDURE ByDates (IN f: TextMappers.Formatter; occurD: A.Tree);
		
		VAR
			
			r: TextRulers.Ruler;
			s: BA.DateTime;
			e: A.Elem;
			l: INTEGER;
			date: LONGINT;
		
		BEGIN (*ByDates*)
			f.rider.SetAttr(TextModels.NewSize(f.rider.attr, corpusSize));
			l := 0;
			e := occurD.Next(NIL);
			WHILE e # NIL DO
				l := MAX(l, f.rider.attr.font.StringWidth(e(PropDate).id$ + '+ '));
				e := occurD.Next(e);
			END;
			r := TextRulers.dir.New(NIL);
			TextRulers.SetJustified(r);
			TextRulers.SetFirst(r, 0);
			TextRulers.SetLeft(r, indent);
			TextRulers.SetFixedRight(r, width);
			TextRulers.AddTab(r, indent + l);
			f.WriteView(r);
			date := - 1;
			e := occurD.Next(NIL);
			WHILE e # NIL DO
				WITH e: PropDate DO
					IF e.date # date THEN
						date := e.date;
						f.rider.SetAttr(TextModels.NewSize(f.rider.attr, headSize));
						f.WriteLn; f.WritePara;
						IF e.date = BA.never THEN
							f.WriteMsg("#Duniter:Never");
						ELSE
							BA.TimestampToString(e.date, s);
							f.WriteString(s);
							IF e.after THEN
								f.WriteString("+");
							END;
						END;
						f.WriteLn;
					END;
					f.rider.SetAttr(TextModels.NewSize(f.rider.attr, corpusSize));
					f.WriteString(e.id);
					f.WriteTab;
					f.WriteString(": "); f.WriteMsg("#Duniter:Proba"); f.WriteString(" = ");
					f.WriteIntForm(e.proba, 10, 3, "", FALSE); (* Digit space *)
					f.WriteString("%");
					f.WriteLn;
				END;
				e := occurD.Next(e);
			END;
		END ByDates;
	
	(* Print the permutation occurN sorted by names *)
	PROCEDURE ByNames (IN f: TextMappers.Formatter; occurN: A.Tree);
		
		VAR
			
			r: TextRulers.Ruler;
			s: BA.DateTime;
			e: A.Elem;
			id: J.StringP;
		
		BEGIN (*ByNames*)
			r := TextRulers.dir.New(NIL);
			TextRulers.SetJustified(r);
			TextRulers.SetFirst(r, 0);
			TextRulers.SetLeft(r, indent);
			TextRulers.SetFixedRight(r, width);
			f.rider.SetAttr(TextModels.NewSize(f.rider.attr, corpusSize));
			BA.TimestampToString(0, s);
			TextRulers.AddTab(r, indent + f.rider.attr.font.StringWidth(s + '+ '));
			f.WriteView(r);
			NEW(id, 1); id^ := 0X;
			e := occurN.Next(NIL);
			WHILE e # NIL DO
				WITH e: PropName DO
					IF e.id$ # id$ THEN
						id := e.id;
						f.rider.SetAttr(TextModels.NewSize(f.rider.attr, headSize));
						f.WriteLn; f.WritePara;
						f.WriteString(e.id);
						f.WriteLn;
					END;
					f.rider.SetAttr(TextModels.NewSize(f.rider.attr, corpusSize));
					IF e.date = BA.never THEN
						f.WriteMsg("#Duniter:Never");
					ELSE
						BA.TimestampToString(e.date, s);
						f.WriteString(s);
						IF e.after THEN
							f.WriteString("+");
						END;
					END;
					f.WriteTab;
					f.WriteString(": "); f.WriteMsg("#Duniter:Proba"); f.WriteString(" = ");
					f.WriteIntForm(e.proba, 10, 3, "", FALSE); (* Digit space *)
					f.WriteString("%");
					f.WriteLn;
				END;
				e := occurN.Next(e);
			END;
		END ByNames;
	
	(* Print the metadata m with the help of f *)
	PROCEDURE Meta (IN f: TextMappers.Formatter; m: J.Json);
		
		CONST
			
			tabNb = 10;
		
		VAR
			
			r: TextRulers.Ruler;
			i: INTEGER;
		
		BEGIN (*Meta*)
			r := TextRulers.dir.New(NIL);
			TextRulers.SetJustified(r);
			TextRulers.SetFirst(r, 0);
			TextRulers.SetLeft(r, 0);
			TextRulers.SetFixedRight(r, width);
			f.rider.SetAttr(TextModels.NewSize(f.rider.attr, corpusSize));
			FOR i := 1 TO tabNb DO
				TextRulers.AddTab(r, i * indentM);
			END;
			f.WriteView(r);
			IF m # NIL THEN
				m.Write(f);
			END;
		END Meta;
	
	PROCEDURE InitProp (VAR p: Controls.Prop);
		
		BEGIN (*InitProp*)
			NEW(p);
			p.link := ""; p.label := ""; p.guard := ""; p.notifier := "";
			p.level := 0;
			p.opt[0] := FALSE; p.opt[1] := FALSE;
			p.opt[2] := FALSE; p.opt[3] := FALSE;
			p.opt[4] := FALSE;
		END InitProp;
	
	(* Write the text of the View v *)
	PROCEDURE (v: View) WriteText, NEW;
		
		VAR
			
			t: TextModels.Model;
			f: TextMappers.Formatter;
			r: TextRulers.Ruler;
		
		(* Insert into the text the three radio buttons and one or two command buttons *)
		PROCEDURE InsertControls (IN f: TextMappers.Formatter);
			
			VAR
				
				r: TextRulers.Ruler;
				prop: Controls.Prop;
				c: Controls.Control;
				s: Dialog.String;
				w, h: INTEGER;
			
			BEGIN (*InsertControls*)
				f.WriteLn;
				r := TextRulers.dir.New(NIL);
				TextRulers.AddTab(r, firstCtrlPos);
				TextRulers.MakeCenterTab(r);
				TextRulers.AddTab(r, secondCtrlPos);
				TextRulers.MakeCenterTab(r);
				TextRulers.AddTab(r, thirdCtrlPos);
				TextRulers.MakeCenterTab(r);
				f.WriteView(r);
				f.WriteTab;
				InitProp(prop);
				prop.link := "DuniterWWViews.dateNameMeta";
				prop.label := "#Duniter:ByName";
				prop.notifier := "DuniterWWViews.DateOrNameNotifier";
				prop.level := 1;
				Dialog.MapString(prop.label, s);
				c := Controls.dir.NewRadioButton(prop);
				f.WriteView(c);
				c.context.GetSize(w, h);
				c.context.SetSize(c.font.StringWidth(s) + radioOffset, h);
				f.WriteTab;
				InitProp(prop);
				prop.link := "DuniterWWViews.dateNameMeta";
				prop.label := "#Duniter:Meta";
				prop.notifier := "DuniterWWViews.DateOrNameNotifier";
				prop.level := 2;
				Dialog.MapString(prop.label, s);
				c := Controls.dir.NewRadioButton(prop);
				f.WriteView(c);
				c.context.GetSize(w, h);
				c.context.SetSize(c.font.StringWidth(s) + radioOffset, h);
				f.WriteTab;
				InitProp(prop);
				prop.link := "DuniterWWViews.dateNameMeta";
				prop.label := "#Duniter:ByDate";
				prop.notifier := "DuniterWWViews.DateOrNameNotifier";
				prop.level := 0;
				Dialog.MapString(prop.label, s);
				c := Controls.dir.NewRadioButton(prop);
				f.WriteView(c);
				c.context.GetSize(w, h);
				c.context.SetSize(c.font.StringWidth(s) + radioOffset, h);
				f.WriteLn;
				f.WriteLn;
				f.WriteTab;
				IF v.modified THEN
					InitProp(prop);
					prop.link := "DuniterWWViews.Clear";
					prop.label := "#Duniter:Check";
					Dialog.MapString(prop.label, s);
					c := Controls.dir.NewPushButton(prop);
					f.WriteView(c);
					c.context.GetSize(w, h);
					c.context.SetSize(c.font.StringWidth(s) + buttonOffset, h);
				END;
				f.WriteTab; f.WriteTab;
				InitProp(prop);
				prop.link := "DuniterWWViews.UpdateAll";
				prop.label := "#Duniter:Update";
				prop.guard := "DuniterWWViews.UpdateAllGuard";
				Dialog.MapString(prop.label, s);
				c := Controls.dir.NewPushButton(prop);
				f.WriteView(c);
				c.context.GetSize(w, h);
				c.context.SetSize(c.font.StringWidth(s) + buttonOffset, h);
				f.WriteLn;
			END InsertControls;
		
		BEGIN (*WriteText*)
			t := v.text.ThisModel();
			t.Delete(0, t.Length());
			f.ConnectTo(t);
			r := TextRulers.dir.New(NIL);
			TextRulers.SetCentered(r);
			TextRulers.SetFixedRight(r, width);
			f.WriteView(r);
			f.rider.SetAttr(TextModels.NewWeight(f.rider.attr, Fonts.normal));
			f.rider.SetAttr(TextModels.NewSize(f.rider.attr, titleSize));
			IF v.modified THEN
				f.WriteString("* ");
			END;
			f.WriteMsg(title);
			IF v.modified THEN
				f.WriteString(" *");
			END;
			f.WriteLn;
			f.rider.SetAttr(TextModels.NewSize(f.rider.attr, headSize));
			f.WriteString(v.nowS);
			f.WriteString(" (UTC+0)");
			f.WriteLn;
			InsertControls(f);
			f.rider.SetAttr(TextModels.NewSize(f.rider.attr, corpusSize));
			IF ~v.occurD.IsEmpty() THEN
				CASE dateNameMeta OF
					|byDate:
						ByDates(f, v.occurD);
					|byName:
						ByNames(f, v.occurN);
					|meta:
						Meta(f, v.meta);
				END;
			END;
			Views.Update(v, Views.keepFrames);
			SetNotDirty(v);
		END WriteText;
	
	(* When the WotWizard lists have been modified, stores on disk the old results versions in oldEntriesName and oldMetaName files *)
	PROCEDURE OldResults (occurD: A.Tree; meta: J.Json);
		
		VAR
			
			t: TextModels.Model;
			f: TextMappers.Formatter;
			v: Views.View;
			loc: Files.Locator;
		
		PROCEDURE Header (VAR f: TextMappers.Formatter);
			
			VAR
				
				r: TextRulers.Ruler;
				prop: Controls.Prop;
				c: Controls.Control;
				s: Dialog.String;
				w, h: INTEGER;
			
			BEGIN (*Header*)
				r := TextRulers.dir.New(NIL);
				TextRulers.SetCentered(r);
				TextRulers.SetFixedRight(r, width);
				f.WriteView(r);
				f.rider.SetAttr(TextModels.NewWeight(f.rider.attr, Fonts.normal));
				f.rider.SetAttr(TextModels.NewSize(f.rider.attr, titleSize));
				f.WriteMsg(title);
				f.WriteLn;
				f.rider.SetAttr(TextModels.NewSize(f.rider.attr, headSize));
				f.WriteMsg("#Duniter:PreviousDisplay");
				f.WriteLn;
				f.WriteLn;
				r := TextRulers.dir.New(NIL);
				TextRulers.AddTab(r, thirdCtrlPos);
				TextRulers.MakeCenterTab(r);
				f.WriteView(r);
				f.WriteTab;
				InitProp(prop);
				prop.link := "DevSearch.Compare";
				prop.label := "#Duniter:Compare";
				Dialog.MapString(prop.label, s);
				c := Controls.dir.NewPushButton(prop);
				f.WriteView(c);
				c.context.GetSize(w, h);
				c.context.SetSize(c.font.StringWidth(s) + buttonOffset, h);
				f.WriteLn;
			END Header;
		
		BEGIN (*OldResults*)
			loc := Files.dir.This(""); ASSERT(loc.res = 0, 100);
			
			t := TextModels.dir.New();
			f.ConnectTo(t);
			Header(f);
			ByDates(f, occurD);
			v := TextViews.dir.New(t);
			Views.RegisterView(v, loc, oldEntriesName);
			
			t := TextModels.dir.New();
			f.ConnectTo(t);
			Header(f);
			Meta(f, meta);
			v := TextViews.dir.New(t);
			Views.RegisterView(v, loc, oldMetaName);
		END OldResults;
	
	(* Extract now and build the permutations oD, sorted by dates, and oN, sorted by names, from j *)
	PROCEDURE BuildOccurDM (j: J.Json; OUT now: BA.DateTime; OUT oD, oN: A.Tree);
		
		VAR
			
			f1, f2, f3: J.Fields;
			d, n: J.Values;
			i, k, m: INTEGER;
			pD: PropDate;
			pM: PropName;
			e: A.Elem;
			b: BOOLEAN;
		
		BEGIN (*BuildOccurDM*)
			A.New(oD); A.New(oN);
			IF j = NIL THEN
				now := "";
			ELSE
				WITH j: J.Object DO
					f1 := j.fields;
					ASSERT(f1[0].name$ = "now", 100);
					ASSERT(f1[0].value IS J.String, 101);
					now := f1[0].value(J.String).s$;
					ASSERT(f1[1].name$ = "dates", 102);
					ASSERT(f1[1].value IS J.JsonVal, 103);
					ASSERT(f1[1].value(J.JsonVal).json IS J.Array, 104);
					d := f1[1].value(J.JsonVal).json(J.Array).elements;
					FOR i := 0 TO LEN(d) - 1 DO
						ASSERT(d[i] IS J.JsonVal, 105);
						ASSERT(d[i](J.JsonVal).json IS J.Object, 106);
						f2 := d[i](J.JsonVal).json(J.Object).fields;
						ASSERT(f2[0].name$ = "after", 107);
						ASSERT(f2[0].value IS J.Bool, 108);
						ASSERT(f2[1].name$ = "date", 109);
						ASSERT(f2[1].value IS J.String, 110);
						ASSERT(f2[2].name$ = "names", 111);
						ASSERT(f2[2].value IS J.JsonVal, 112);
						ASSERT(f2[2].value(J.JsonVal).json IS J.Array, 113);
						n := f2[2].value(J.JsonVal).json(J.Array).elements;
						FOR k := 0 TO LEN(n) - 1 DO
							ASSERT(n[k] IS J.JsonVal, 114);
							ASSERT(n[k](J.JsonVal).json IS J.Object, 115);
							f3 := n[k](J.JsonVal).json(J.Object).fields;
							ASSERT(f3[0].name$ = "name", 116);
							ASSERT(f3[0].value IS J.String, 117);
							ASSERT(f3[1].name$ = "proba", 118);
							ASSERT(f3[1].value IS J.Integer, 119);
							NEW(pD);
							pD.after := f2[0].value(J.Bool).bool;
							pD.date := BA.StringToTimestamp(f2[1].value(J.String).s$);
							pD.id := f3[0].value(J.String).s;
							pD.proba := SHORT(f3[1].value(J.Integer).n);
							e := pD;
							b := oD.SearchIns(e, m); ASSERT(~b, 120);
							NEW(pM);
							pM.after := pD.after;
							pM.date := pD.date;
							pM.id := pD.id;
							pM.proba := pD.proba;
							e := pM;
							b := oN.SearchIns(e, m); ASSERT(~b, 121);
						END;
					END;
				END;
			END;
		END BuildOccurDM;
	
	(* Test whether the WotWizard lists have changed *)
	PROCEDURE (v: View) UpdateText, NEW;
		
		VAR
			
			occurD: A.Tree;
			meta: J.Json;
			b, oM: BOOLEAN;
			nowS: BA.DateTime;
		
		BEGIN (*UpdateText*)
			BuildOccurDM(J.Read("", textByDate), nowS, occurD, v.occurN);
			BA.Collect;
			meta := J.Read("", textMeta);
			BA.Collect;
			oM := v.modified;
			b := ~OccursEqual(occurD, v.occurD);
			v.modified := v.modified OR b;
			IF b & ~oM THEN
				OldResults(v.occurD, v.meta);
			END;
			v.nowS := nowS; v.occurD := occurD; v.meta := meta;
		END UpdateText;
	
	PROCEDURE (v: View) InstallText, NEW;
		
		BEGIN (*InstallText*)
			v.text := TextViews.dir.New(TextModels.dir.New());
			Stores.Join(v, v.text);
			v.text.ThisController().SetOpts({Containers.noCaret});
			v.UpdateText;
		END InstallText;
	
	PROCEDURE (v: View) CopyFromModelView (source: Views.View; model: Models.Model);
		
		BEGIN (*CopyFromModelView*)
			WITH source: View DO
				v.occurD := source.occurD.Copy();
				v.occurN := source.occurN.Copy();
				IF model = NIL THEN
					v.text := Views.CopyOf(source.text, Views.deep)(TextViews.View);
				ELSE
					v.text := Views.CopyWithNewModel(source.text, model)(TextViews.View);
				END;
				v.modified := source.modified;
			END;
		END CopyFromModelView;
	
	PROCEDURE (v: View) ThisModel (): Models.Model;
		
		BEGIN (*ThisModel*)
			RETURN v.text.ThisModel();
		END ThisModel;
	
	PROCEDURE (v: View) InitContext (context: Models.Context);
		
		BEGIN (*InitContext*)
			v.InitContext^(context);
			v.text.InitContext(context);
		END InitContext;
	
	PROCEDURE (v: View) Neutralize;
		
		BEGIN (*Neutralize*)
			v.text.Neutralize;
		END Neutralize;
	
	PROCEDURE (v: View) Restore (f: Views.Frame; l, t, r, b: INTEGER);
		
		BEGIN (*Restore*)
			Views.InstallFrame(f, v.text, 0, 0, 0, TRUE);
		END Restore;
	
	PROCEDURE (v: View) HandleViewMsg (f: Views.Frame; VAR msg: Views.Message);
		
		BEGIN (*HandleViewMsg*)
			WITH msg: FindMsg DO
				IF msg.v = NIL THEN
					msg.v := v;
				END;
			ELSE
			END;
		END HandleViewMsg;
	
	PROCEDURE (v: View) HandleCtrlMsg (f: Views.Frame; VAR msg: Controllers.Message; VAR focus: Views.View);
		
		BEGIN (*HandleCtrlMsg*)
			WITH
				|msg: Controllers.PollOpsMsg DO
					msg.type := "DuniterWWViews.View";
					IF v.text.ThisController().HasSelection() THEN
						msg.valid := {Controllers.copy};
					END;
				ELSE
					focus := v.text;
			END;
		END HandleCtrlMsg;
	
	PROCEDURE (v: View) HandlePropMsg (VAR p: Properties.Message);
		
		BEGIN (*HandlePropMsg*)
			WITH 
				|p: Properties.SizePref DO
					IF p.w = Views.undefined THEN
						p.w := width;
					END;
					IF p.h = Views.undefined THEN
						p.h := height;
					END;
				|p: Properties.ResizePref DO
					p.verFitToWin := TRUE;
					p.horFitToWin := TRUE;
				ELSE
					Views.HandlePropMsg(v.text, p);
			END;
		END HandlePropMsg;
	
	PROCEDURE Focus* (): Views.View;
		
		VAR
			
			v: Views.View;
		
		BEGIN (*Focus*)
			v := Controllers.FocusView();
			IF (v # NIL) & (v IS View) THEN
				RETURN v;
			ELSE
				RETURN NIL;
			END;
		END Focus;
	
	PROCEDURE Rewrite;
		
		VAR
			
			v: View;
		
		BEGIN (*Rewrite*)
			v := FindFirst();
			IF v # NIL THEN
				v.WriteText;
			END;
		END Rewrite;
	
	PROCEDURE Update;
		
		VAR
			
			v: View;
		
		BEGIN (*Update*)
			v := FindFirst();
			IF v # NIL THEN
				v.UpdateText;
				v.WriteText;
			END;
		END Update;
	
	(* Remove the asterik marks and the button "Check"; open the new and old versions of metadata and entries data for comparisons *)
	PROCEDURE Clear*;
		
		VAR
			
			v: View;
			vv: TextViews.View;
			vvo: Views.View;
			loc: Files.Locator;
			dN: INTEGER;
		
		BEGIN (*Clear*)
			v := FindFirst();
			IF v # NIL THEN
				v.modified := FALSE;
				v.WriteText;
				IF showOldEntries THEN
					loc := Files.dir.This(""); ASSERT(loc.res = 0, 100);
					
					vvo := Views.OldView(loc, oldMetaName);
					IF vvo # NIL THEN
						IF dateNameMeta # meta THEN
							dN := dateNameMeta;
							dateNameMeta := meta;
							Rewrite;
							vv := Stores.CopyOf(v.text)(TextViews.View);
							dateNameMeta := dN;
							Rewrite;
						ELSE
							vv := Stores.CopyOf(v.text)(TextViews.View);
						END;
						Views.OpenView(vv);
						StdCmds.SetEditMode;
						SetNotDirty(vv);
						Views.OpenView(vvo);
						DevSearch.Compare;
					END;
					
					vvo := Views.OldView(loc, oldEntriesName);
					IF vvo # NIL THEN
						IF dateNameMeta # byDate THEN
							dN := dateNameMeta;
							dateNameMeta := byDate;
							Rewrite;
							vv := Stores.CopyOf(v.text)(TextViews.View);
							dateNameMeta := dN;
							Rewrite;
						ELSE
							vv := Stores.CopyOf(v.text)(TextViews.View);
						END;
						Views.OpenView(vv);
						StdCmds.SetEditMode;
						SetNotDirty(vv);
						Views.OpenView(vvo);
						DevSearch.Compare;
					END;
					
				END;
			END;
		END Clear;
	
	(* Mark the view as modified *)
	PROCEDURE SetModified*;
		
		VAR
			
			v: View;
		
		BEGIN (*SetModified*)
			v := FindFirst();
			IF v # NIL THEN
				v.modified := TRUE;
				v.WriteText;
			END;
		END SetModified;
	
	PROCEDURE DateOrNameNotifier* (op, from, to: INTEGER);
		
		BEGIN (*DateOrNameNotifier*)
			IF op = Dialog.changed THEN
				Rewrite;
			END;
		END DateOrNameNotifier;
	
	PROCEDURE ShowOldEntries*;
		
		BEGIN (*ShowOldEntries*)
			showOldEntries := TRUE;
		END ShowOldEntries;
	
	PROCEDURE HideOldEntries*;
		
		BEGIN (*HideOldEntries*)
			showOldEntries := FALSE;
		END HideOldEntries;
	
	PROCEDURE New* (): Views.View;
		
		VAR
			
			v: View;
		
		BEGIN (*New*)
			NEW(v);
			A.New(v.occurD);
			v.InstallText;
			v.modified := FALSE;
			v.WriteText;
			RETURN v;
		END New;
	
	PROCEDURE Deposit*;
		
		BEGIN (*Deposit*)
			Views.Deposit(New());
		END Deposit;
	
	PROCEDURE Open*;
		
		BEGIN (*Open*)
			Views.OpenAux(New(), title);
		END Open;
	
	PROCEDURE UpdateAll*;
		
		BEGIN (*UpdateAll*)
			IF ~running THEN
				Services.RemoveAction(nC);
				Services.RemoveAction(vC);
				Services.DoLater(nC, Services.now);
			END;
		END UpdateAll;
	
	PROCEDURE UpdateAllGuard* (VAR par: Dialog.Par);
		
		BEGIN (*UpdateAllGuard*)
			par.disabled := running;
		END UpdateAllGuard;
	
	PROCEDURE (c: NewCalculus) Do;
		
		PROCEDURE MarkRunning;
			
			VAR
				
				loc: Files.Locator;
				f: Files.File;
				res: INTEGER;
			
			BEGIN (*MarkRunning*)
				loc := Files.dir.This(rsrcDir); ASSERT(loc.res = 0, 100);
				f := Files.dir.New(loc, Files.dontAsk); ASSERT(loc.res = 0, 101);
				f.Register(runningName, "", Files.dontAsk, res); ASSERT(res = 0, 102);
				running := TRUE;
			END MarkRunning;
		
		BEGIN (*Do*)
			IF running THEN
				Services.DoLater(c, Services.Ticks() + verifyPeriod); (* Risque de blocage ; à voir *)
			ELSE
				Services.DoLater(c, Services.Ticks() + calculusPeriod);
				MarkRunning;
				BA.Collect; (* Free the File in MarkRunning *)
				UtilExternal.Start("WWServer.exe");
				Services.DoLater(vC, Services.Ticks() + verifyPeriod);
			END;
		END Do;
	
	PROCEDURE (v: VerifyCalcEnd) Do;
		
		VAR
			
			loc: Files.Locator;
			f: Files.File;
		
		BEGIN (*Do*)
			loc := Files.dir.This(rsrcDir); ASSERT(loc.res = 0, 100);
			f := Files.dir.Old(loc, runningName, Files.exclusive);
			IF f = NIL THEN
				Update;
				running := FALSE;
			ELSE
				f.Close;
				Services.DoLater(v, Services.Ticks() + verifyPeriod);
			END;
		END Do;
	
	PROCEDURE Init;
		
		BEGIN (*Init*)
			dateNameMeta := byName;
			showOldEntries := TRUE;
			NEW(vC);
			NEW(nC);
			running := FALSE;
			UpdateAll;
		END Init;
	
	BEGIN (*DuniterWWViews*)
		Init;
	CLOSE
		Services.RemoveAction(nC);
		Services.RemoveAction(vC);
	END DuniterWWViews.

"DuniterWWViews.Deposit;StdCmds.Open"

DuniterBlockchain.UpdateAll;

DuniterWWViews.Clear;

DuniterWWViews.SetModified;

DuniterWWViews.ShowOldEntries;

DuniterWWViews.HideOldEntries;
